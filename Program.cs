﻿using System;

namespace Exercises
{
    class Program
    {
        // Part A method
        static void MyMethod()
        {
            Console.WriteLine("My first method\n");
        }
        // Part B method
        static void PrintName(string _name)
        {
            Console.WriteLine($"Hello {_name}. \n");
        }
        // Part C method
        static string FindName(int index)
        {
            if (index == 0) {   return "Tom";   }
            if (index == 1) {   return "Dick";   }
            if (index == 2) {   return "Harry";   }
            if (index == 3) {   return "Jack";   }
            if (index == 4) {   return "Jill";   }
            return "Index out of bounds.";
        }
        // Part D method
        static float TrinaryMult(float a, float b, float c)
        {
            return a * b * c;
        }

        // Part E method
        static void CountNumbers(int number)
        {
            for (int i = 0; i < number; i++)
            {
                Console.WriteLine(i + 1);
            }
        }

        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            // Part A
            Console.WriteLine("Ex 24 - Part A\n");
            MyMethod();
            MyMethod();
            MyMethod();
            MyMethod();
            MyMethod();
            
            // Part B
            Console.WriteLine("Ex 24 - Part B\n");
            string[] names = new string[] {"Joesf", "John Snow", "Odin", "Thor", "AwesomeName"};
            for (int i = 0; i < names.Length; i++)
            {
                PrintName(names[i]);
            }
            Console.WriteLine();

            // Part C
            Console.WriteLine("Ex 24 - Part C\n");
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(FindName(i));
            }
            Console.WriteLine();

            // Part D
            Console.WriteLine("Ex 24 - Part D\n");
            float a = 5.0f;
            float b = 1.25f;
            float c = 9.56f;
            float d = 7.45f;
            float e = 3.75f;
            float f = 9.99f;

            Console.WriteLine($"Equation 1: {a} * {b} * {c} = {TrinaryMult(a, b, c)}");
            Console.WriteLine($"Equation 2: {d} * {e} * {f} = {TrinaryMult(d, e, f)}");
            Console.WriteLine($"Equation 3: {d} * {a} * {c} = {TrinaryMult(d, a, c)}");
            Console.WriteLine($"Equation 4: {f} * {b} * {e} = {TrinaryMult(f, b, e)}");
            Console.WriteLine($"Equation 5: {f} * {a} * {b} = {TrinaryMult(f, a, b)}");
            Console.WriteLine();

            // Part E
            Console.WriteLine("Ex 24 - Part E\n");
            Console.WriteLine("Please enter a whole number");
            CountNumbers(int.Parse(Console.ReadLine()));

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
